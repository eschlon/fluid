Fluid is Copyright (c) 2017 Ed Holsinger.

Fluid is intended as a public ruleset. In order to facilitate this I felt that two objectives needed to be satisfied.

1. As a user, you should be free to use the source rules to do anything you want.
2. As a creator, you should feel confident that using the system doesn't impinge on your inherent rights to your content.

To support these goals I decided the best option was to release all core system modules under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). This includes the main source module in `/modules/source` as well as any other system modules in `/modules`. This license gives you broad permission to use this content for any purpose as long as you abide by the terms of the license, which functionally just means you just need to provide attribution.

Some modules, specifically `/modules/talus` are relased under a more restrictive license, as that content represents my own setting and content rather than core source rules. Breaking things up into modules this way allows me to define on a piece by piece level which content is fully open and which is more restrictied; however, the nature of the CC-BY license means that *you* don't have to deal with that if you don't want to. If you'd like to use Fluid for your sourcebook or setting you're free to do so, and release the derivative work under whatever license you choose (so long as you provide attribution). See the individual modules licence files for specifics or just go save the [Source Wiki](https://eschlon.gitlab.io/fluid) if you're just looking for the fully open CC-BY content.