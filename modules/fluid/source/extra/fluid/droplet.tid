caption: Fluid Droplets
created: 20231220055229694
icon: droplet
list-after: fluid/srd/Source
modified: 20240115203956445
tags: fluid/sidebar/Rules
title: fluid/Droplet

<<@concept droplet droplets>> define systems for representing <<@concept entity entities>> including player characters, NPCs, monsters, villains, weapons, equipment and all of the important things in a given setting.

Each droplet starts with an objective which explains what sort of thing it is trying to represent and why one might want ot include it in their game. It then goes on to outline the overall system and introduce the primitive primitives and mechanics it needs to function.

When selecting <<@concept droplet droplets>> for your game, you don't need to (and shouldn't) just use everything. Choose the right <<@concept droplet droplets>> to represent the things that are important in your setting and narrative. If you can't find one that fits, modify one that's close or make your own.

! Guiding Principles

<<@fluid>> is fundamentally a system intended to provide a framework around collaborative storytelling. The rules in <<@link fluid/srd icon:yes>> are that framework, but they make very few assumptions about what is important to //your// setting and narrative. <<@concept droplet droplets>> allow you to add mechancial focus on those important things, but a <<@concept droplet>> is no substitute for good old-fashioned world building.

A good <<@concept droplet>> is one that aligns with the setting and narrative. All <<@concept droplet droplets>> add mechancial complexity, and mechanics always draw attention to themselves. Done properly this can provide structure and consistency to the setting, opportunities for customization and growth, and focus narrative attention toward what's important for the setting. In general <<@concept droplet droplets>>:

* Provide representations of <<@concept entity entities>>
* Should leverage existing <<@concept primitive primitives>> rather than introducing new ones
* Should avoid overriding <<@link fluid/srd icon:yes>>