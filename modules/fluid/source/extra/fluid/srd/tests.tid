created: 20231228134759389
icon: $:/plugins/fluid/source/icon
list: fluid/srd/Invocations fluid/srd/Boons fluid/srd/Advantages fluid/srd/Burdens fluid/srd/Difficulty
modified: 20240118045108819
title: fluid/srd/Tests

<<@concept test tests>> are made by rolling a pool of ten-sided <<@concept dice>> and counting <<@concept success successes>> -- <<@concept dice>> with a value above some <<@concept threshold>>. These <<@concept success successes>> can then be bid against the <<@concept difficulty>> of your <<@concept goal>> to determine who has <<@concept narrator "narrative control">>.

* You roll a pool of ten-sided <<@concept dice>>. The number of <<@concept dice>> and any modifiers to your roll are determined by invoking one or more of primitives
* You compare the value of each die to a <<@concept threshold>>. The default <<@concept threshold>> is $$6$$ (a $$50/50$$ chance per die)
* You bid your <<@concept success successes>> against the <<@concept difficulty>> of your <<@concept goal>>. If the <<@concept difficulty>> of your <<@concept goal>> is <<@D 4>> you need to bid //at least// <<@s 4>> to become <<@concept narrator>>.
* The number of <<@concept success successes>> above or below the <<@concept difficulty>> indicates how you performed
** If you bid less than the <<@concept difficulty>> then you lose control of the situation by a <<@concept margin>> equal to the difference. 
** If you bid //at least// as much as the <<@concept difficulty>> then you gain control with an <<@concept influence>> equal to the difference.

<<<.example
Jonah's party is on a leisurely hunt with the King when they are attacked by a group of murderous goblins. Jonah decides that her primary objective is to draw the monsters away from the King and the GM sets a difficulty of <<@D 3>> for doing so. Jonah draws her sword and invokes her <<@facet melee 9 icon:facet link:no>> to get a pool of <<@d 9>> and rolls <<@s 6>>, more than enough. 

She needs to bid //at least// <<@s 3>> to take control and draw the Goblins away from the king.
<<<